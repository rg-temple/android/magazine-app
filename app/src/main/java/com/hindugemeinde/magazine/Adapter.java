package com.hindugemeinde.magazine;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.github.barteksc.pdfviewer.PDFView;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

public class Adapter extends RecyclerView.Adapter<ViewHolder> {
    public static String filename;
    MainActivity mainActivity;
    ArrayList<Data> data;

    public Adapter(MainActivity mainActivity, ArrayList<Data> data) {
        this.mainActivity = mainActivity;
        this.data = data;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mainActivity.getBaseContext());
        View view = layoutInflater.inflate(R.layout.listelements, null, false);

        return new ViewHolder(view);
    }

    public void downloadFile(Context context, String filename, String extension, String destination, String url) {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(destination, filename + extension);
        request.setMimeType("application/pdf");
        downloadManager.enqueue(request);
        Log.d("Download", destination + "/" + filename + extension);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.name.setText(data.get(position).getName());
        holder.date.setText(data.get(position).getDate());

        SpannableString content = new SpannableString("Link (click to copy to clipboard)");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        holder.link.setText(content);
        holder.link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboardManager = (ClipboardManager) mainActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Link", data.get(position).getLink());
                clipboardManager.setPrimaryClip(clip);
                Toast.makeText(mainActivity, "Copied link", Toast.LENGTH_SHORT).show();
            }
        });
        holder.link.setMovementMethod(LinkMovementMethod.getInstance());
        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile(holder.name.getContext(), data.get(position).getName(), ".pdf", DIRECTORY_DOWNLOADS, data.get(position).getLink());
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filename = data.get(position).getName();
                Log.d("TAG", (String) filename);
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + filename.replaceAll(" ", "\\ ") + ".pdf");
                Uri uri = FileProvider.getUriForFile(holder.name.getContext(), "com.hindugemeinde.magazine" + ".provider", file);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                mainActivity.startActivity(intent);
                /*
                filename = data.get(position).getName();
                Intent intent = new Intent(holder.name.getContext(), PDFViewer.class);
                mainActivity.startActivity(intent);
                */
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
