package com.hindugemeinde.magazine;

public class Data {
    public String name, link, date;

    public Data(String name, String link, String date) {
        this.name = name;
        this.link = link;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
