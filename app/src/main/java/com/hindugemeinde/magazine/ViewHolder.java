package com.hindugemeinde.magazine;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolder extends RecyclerView.ViewHolder {
    public TextView name, date, link;
    public Button download, view;
    public ViewHolder(@NonNull @org.jetbrains.annotations.NotNull View itemView) {
        super(itemView);

        name = itemView.findViewById(R.id.name);
        date = itemView.findViewById(R.id.date);
        link = itemView.findViewById(R.id.link);
        download = itemView.findViewById(R.id.download);
        view = itemView.findViewById(R.id.view);
    }
}
